﻿using PredictSat.Core.Domain.Predictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PredictSat.Web.ViewModels
{
    public class PredictionReportConfigViewModel
    {
        public IEnumerable<SelectListItem> PredictionsMenu { get; set; }

        public IEnumerable<SelectListItem> QueryTypeMenu { get; set; }
    }
}