﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PredictSat.Web.ViewModels
{
    public class PredictionCompleteViewModel
    {
        public PredictionCompleteViewModel()
        {

        }


        //input
        public int Id { get; set; }

        public string UserId { get; set; }

        public string SchoolName { get; set; }

        public DateTime ForYear { get; set; }

        public int NumberTestTakers { get; set; }

        public double PeerIndex { get; set; }

        public double PerformanceCategoryScore { get; set; }

        public double NumberOfClassesMean { get; set; }

        public double SchoolWideTeacherPupilRation { get; set; }

        public double AvgClassSizeMean { get; set; }

        public double ProgressCategoryScore { get; set; }

        public double SizeOfSmallestClass { get; set; }

        public double TotalRegisterMean { get; set; }

        public double EnviromentCategoryScore { get; set; }

        //output
        public double CriticalReadingMean { get; set; }

        public double MathematicsMean { get; set; }

        public double WritingMean { get; set; }
    }
}