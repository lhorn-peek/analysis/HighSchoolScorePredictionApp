﻿using PredictSat.Core.Domain.Predictions;
using PredictSat.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PredictSat.Web.Adapters
{
    public class PredictionCompleteViewModelAdapter
    { 
        public static PredictionCompleteViewModel ToViewModel(SatPredictionInput input, SatPredictionResult output)
        {
            if (input == null || output == null)
                return null;

            var viewModel = new PredictionCompleteViewModel() 
            {
                Id = input.Id,
                AvgClassSizeMean = input.AvgClassSizeMean,
                CriticalReadingMean = output.CriticalReadingMean,
                EnviromentCategoryScore = input.EnviromentCategoryScore,
                ForYear = input.ForYear,
                MathematicsMean = output.MathematicsMean,
                NumberOfClassesMean = input.NumberOfClassesMean,
                NumberTestTakers = input.NumberTestTakers,
                PeerIndex = input.PeerIndex,
                PerformanceCategoryScore = input.PerformanceCategoryScore,
                ProgressCategoryScore = input.ProgressCategoryScore,
                SchoolName = input.SchoolName,
                SchoolWideTeacherPupilRation = input.SchoolWideTeacherPupilRation,
                SizeOfSmallestClass = input.SizeOfSmallestClass,
                TotalRegisterMean = input.TotalRegisterMean,
                UserId = input.UserId,
                WritingMean = output.WritingMean
            };

            return viewModel;
        }


        public static PredictionCompleteViewModel ToViewModel(SatPredictionInput input)
        {
            if (input == null)
                return null;

            var viewModel = new PredictionCompleteViewModel()
            {
                Id = input.Id,
                AvgClassSizeMean = input.AvgClassSizeMean,
                CriticalReadingMean = input.PredictionResult.CriticalReadingMean,
                EnviromentCategoryScore = input.EnviromentCategoryScore,
                ForYear = input.ForYear,
                MathematicsMean = input.PredictionResult.MathematicsMean,
                NumberOfClassesMean = input.NumberOfClassesMean,
                NumberTestTakers = input.NumberTestTakers,
                PeerIndex = input.PeerIndex,
                PerformanceCategoryScore = input.PerformanceCategoryScore,
                ProgressCategoryScore = input.ProgressCategoryScore,
                SchoolName = input.SchoolName,
                SchoolWideTeacherPupilRation = input.SchoolWideTeacherPupilRation,
                SizeOfSmallestClass = input.SizeOfSmallestClass,
                TotalRegisterMean = input.TotalRegisterMean,
                UserId = input.UserId,
                WritingMean = input.PredictionResult.WritingMean
            };

            return viewModel;
        }

        public static SatPredictionInput FromViewModel(PredictionCompleteViewModel viewModel)
        {
            if (viewModel == null)
                return null;

            //input
            return new SatPredictionInput()
            {
                Id = viewModel.Id,
                AvgClassSizeMean = viewModel.AvgClassSizeMean,
                EnviromentCategoryScore = viewModel.EnviromentCategoryScore,
                ForYear = viewModel.ForYear,
                NumberOfClassesMean = viewModel.NumberOfClassesMean,
                NumberTestTakers = viewModel.NumberTestTakers,
                PeerIndex = viewModel.PeerIndex,
                PerformanceCategoryScore = viewModel.PerformanceCategoryScore,
                ProgressCategoryScore = viewModel.ProgressCategoryScore,
                SchoolName = viewModel.SchoolName,
                SchoolWideTeacherPupilRation = viewModel.SchoolWideTeacherPupilRation,
                SizeOfSmallestClass = viewModel.SizeOfSmallestClass,
                TotalRegisterMean = viewModel.TotalRegisterMean,
                UserId = viewModel.UserId,
                //output
                PredictionResult = new SatPredictionResult() { 
                    WritingMean = viewModel.WritingMean,
                    CriticalReadingMean = viewModel.CriticalReadingMean,
                    MathematicsMean = viewModel.MathematicsMean
                }
            };
        }
    }
}