﻿/// <reference path="jquery-1.10.2.js" />

/// <reference path="jquery-1.10.2.intellisense.js" />

/// <reference path="highcharts.js" />




var ReportBuilder = (function () {

    function ReportBuilder() {

    }

    ReportBuilder.prototype.GetQueryData = function() {

        var queryParameters = {

            MinMax: false,
            PredictionIds: [],
            UserId: 0
        };

        var selectedQueryType = $("#queryTypeDropdown option:selected").text();

        if (selectedQueryType == 'Min Max Scores') {

            queryParameters.MinMax = true;

        }
        else {

            var ids_str = $('#predictionsDropdown').val();

            for (var i = 0; i < ids_str.length; i++) {

                var num = parseInt(ids_str[i], 10);
                queryParameters.PredictionIds.push(num);
            }

        }

        return queryParameters;
    };

    ReportBuilder.prototype.DrawReport = function (data) {

        if (data[0].hasOwnProperty("avg_score"))
            this.DrawMinMaxReport(data)
        else
            this.DrawCompareReport(data);
    }

    ReportBuilder.prototype.DrawMinMaxReport = function (data) {

        var min, max, minTitle, maxTitle;

        if (data[0].avg_score > data[1].avg_score){

            max = [data[0].NumberTestTakers, data[0].PeerIndex, data[0].PerformanceCategoryScore, data[0].NumberOfClassesMean, data[0].SchoolWideTeacherPupilRation, data[0].AvgClassSizeMean, data[0].ProgressCategoryScore, data[0].SizeOfSmallestClass, data[0].TotalRegisterMean, data[0].EnviromentCategoryScore, data[0].CriticalReadingMean, data[0].MathematicsMean, data[0].WritingMean];
            min = [data[1].NumberTestTakers, data[1].PeerIndex, data[1].PerformanceCategoryScore, data[1].NumberOfClassesMean, data[1].SchoolWideTeacherPupilRation, data[1].AvgClassSizeMean, data[1].ProgressCategoryScore, data[1].SizeOfSmallestClass, data[1].TotalRegisterMean, data[1].EnviromentCategoryScore, data[1].CriticalReadingMean, data[1].MathematicsMean, data[1].WritingMean];

            maxTitle = "Highest Score - " + data[0].SchoolName + ", " + data[0].ForYear;
            minTitle = "Lowest Score - " + data[1].SchoolName + ", " + data[1].ForYear;
        }
        else {

            min = [data[0].NumberTestTakers, data[0].PeerIndex, data[0].PerformanceCategoryScore, data[0].NumberOfClassesMean, data[0].SchoolWideTeacherPupilRation, data[0].AvgClassSizeMean, data[0].ProgressCategoryScore, data[0].SizeOfSmallestClass, data[0].TotalRegisterMean, data[0].EnviromentCategoryScore, data[0].CriticalReadingMean, data[0].MathematicsMean, data[0].WritingMean];
            max = [data[1].NumberTestTakers, data[1].PeerIndex, data[1].PerformanceCategoryScore, data[1].NumberOfClassesMean, data[1].SchoolWideTeacherPupilRation, data[1].AvgClassSizeMean, data[1].ProgressCategoryScore, data[1].SizeOfSmallestClass, data[1].TotalRegisterMean, data[1].EnviromentCategoryScore, data[1].CriticalReadingMean, data[1].MathematicsMean, data[1].WritingMean];

            maxTitle = "Highest Score - " + data[1].SchoolName + ", " + data[1].ForYear;
            minTitle = "Lowest Score - " + data[0].SchoolName + ", " + data[0].ForYear;
        }

        $('#reportContainer').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Highest & Lowest Performing'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ["NumberTestTakers", "PeerIndex", "PerformanceCategoryScore", "NumberOfClassesMean", "SchoolWideTeacherPupilRation", "AvgClassSizeMean", "ProgressCategoryScore", "SizeOfSmallestClass", "TotalRegisterMean", "EnviromentCategoryScore", "CriticalReadingMean", "MathematicsMean", "WritingMean"],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: minTitle,
                data: min

            }, {
                name: maxTitle,
                data: max

            }]
        });
    }

    ReportBuilder.prototype.DrawCompareReport = function (data) {

        var series = [];

        for (var i = 0; i < data.length; i++) {

            var ser = {
                name: data[i].SchoolName + ", " + data[i].ForYear,
                data : [data[i].NumberTestTakers, data[i].PeerIndex, data[i].PerformanceCategoryScore, data[i].NumberOfClassesMean, data[i].SchoolWideTeacherPupilRation, data[i].AvgClassSizeMean, data[i].ProgressCategoryScore, data[i].SizeOfSmallestClass, data[i].TotalRegisterMean, data[i].EnviromentCategoryScore, data[i].CriticalReadingMean, data[i].MathematicsMean, data[i].WritingMean]
            };

            series.push(ser);
        }

        $('#reportContainer').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Compare n Predictions'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ["NumberTestTakers", "PeerIndex", "PerformanceCategoryScore", "NumberOfClassesMean", "SchoolWideTeacherPupilRation", "AvgClassSizeMean", "ProgressCategoryScore", "SizeOfSmallestClass", "TotalRegisterMean", "EnviromentCategoryScore", "CriticalReadingMean", "MathematicsMean", "WritingMean"],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: series
        });
    }

    return ReportBuilder;

})();







var repBuilder;

$(document).ready(function () {

    repBuilder = new ReportBuilder();

    $("#btnCreateReport").click(function () {

        var queryParameters = repBuilder.GetQueryData();

        $.ajax({

            url: 'PredictionReport/ExecuteQuery?queryParameters=' + JSON.stringify(queryParameters),
            type: 'POST',
            //dataType: 'json',
            //contentType: 'application/json',
            //data: JSON.stringify(queryParameters)
            

        }).done(function (data) {

            repBuilder.DrawReport(data);

        }).fail(function (jqXHR, textStatus) {

            alert("Request Failed: " + textStatus);
        });
    });
});




//function GetQueryData () {

//    var queryParameters = {

//        MinMax: false,
//        PredictionIds: [],
//        UserId: 0
//    };

//    var selectedQueryType = $("#queryTypeDropdown option:selected").text();

//    if (selectedQueryType == 'Min Max Scores') {

//        queryParameters.MinMax = true;

//    }
//    else
//    {

//        var ids_str = $('#predictionsDropdown').val();

//        for (var i = 0; i < ids_str.length; i++) {

//            var num = parseInt(ids_str[i], 10);
//            queryParameters.PredictionIds.push(num);
//        }

//    }

//    return queryParameters;
//};