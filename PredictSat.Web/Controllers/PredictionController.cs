﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PredictSat.Services.Predictions;
using PredictSat.Core.Domain.Predictions;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using PredictSat.Web.ViewModels;
using PredictSat.Web.Adapters;

namespace PredictSat.Web.Controllers
{
    [Authorize]
    public class PredictionController : Controller
    {
        private ISatPredictionService _predictionService;

        public PredictionController(ISatPredictionService predictionService)
        {
            _predictionService = predictionService;
        }



        //
        // GET: /Prediction/
        
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();

            var predictions = _predictionService.GetAllPrections(userId);

            return View(predictions);
        }


        //
        // GET: /Prediction/Create
        [HttpGet]
        public ActionResult Create(PredictionCompleteViewModel model)
        {
            return View(model);
        }

        //
        // POST: /Prediction/Create
        [HttpPost]
        public ActionResult Save(PredictionCompleteViewModel model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Create", model);


            try
            {
                model.UserId = User.Identity.GetUserId();
                // TODO: Add insert logic here
                _predictionService.InsertPrediction(PredictionCompleteViewModelAdapter.FromViewModel(model));

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }


        public ActionResult MakePrediction()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MakePrediction(SatPredictionInput prediction)
        {
            if (!ModelState.IsValid)
                return View(prediction);

            try
            {
                // TODO: Add insert logic here
                var predictionResult = _predictionService.MakePrediction(prediction);

                var predictionComplete = PredictionCompleteViewModelAdapter.ToViewModel(prediction, predictionResult);

                return RedirectToAction("Create", predictionComplete);
            }
            catch
            {
                return View("Error");
            }

        }

        //
        // GET: /Prediction/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Prediction/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        
        public ActionResult Details(int id)
        {
            var prediction = _predictionService.GetPredictionInputAndResultById(id);

            var viewModel = PredictionCompleteViewModelAdapter.ToViewModel(prediction);

            return View(viewModel);
        }

        //
        // GET: /Prediction/Delete/5
        public ActionResult Delete(int id)
        {
            var prediction = _predictionService.GetPredictionInputAndResultById(id);

            var viewModel = PredictionCompleteViewModelAdapter.ToViewModel(prediction);

            return View(viewModel);
        }

        //
        // POST: /Prediction/Delete/5
        [HttpPost]
        public ActionResult Remove(int id)
        {
            
            //try
            //{
                // TODO: Add delete logic here
                _predictionService.DeletePredictionById(id);

                return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View("Error");
            //}
        }
    }
}
