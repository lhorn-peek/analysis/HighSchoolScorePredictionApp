﻿using PredictSat.Services.Queries.ComparePredictions;
using PredictSat.Web.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PredictSat.Web.ViewModels;
using PredictSat.Services.Predictions;
using Newtonsoft.Json;

namespace PredictSat.Web.Controllers
{
    [Authorize]
    public class PredictionReportController : Controller
    {
        private ComparePredictionsReportFacade _reportFacade;
        private ISatPredictionService _predictionService;

        public PredictionReportController(ComparePredictionsReportFacade reportFacade, 
            ISatPredictionService predictionService)
        {
            _reportFacade = reportFacade;
            _predictionService = predictionService;
        }

        //
        // GET: /PredictionReport/
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();

            var configModel = SetupClientConfig(userId);

            return View(configModel);
        }


        [HttpPost]
        public ActionResult ExecuteQuery(string queryParameters)
        {
            var queryParams = (QueryParameters)JsonConvert.DeserializeObject(queryParameters, typeof(QueryParameters));

            queryParams.UserId = User.Identity.GetUserId();

            var result = _reportFacade.ExecuteQuery(queryParams);

            return Json(result);
        }

        private PredictionReportConfigViewModel SetupClientConfig(string userId)
        {
            var queryTypes = new string[] { "Compare n Predictions", "Min Max Scores" };

            var userPredictions = _predictionService.GetAllPrections(userId).Select(x => new SelectListItem() 
            {
                Text = string.Format("{0}, {1}", x.SchoolName, x.ForYear.ToShortDateString()),
                Value = x.Id.ToString()
            });

            var config = new PredictionReportConfigViewModel()
            {

                PredictionsMenu = userPredictions,
                QueryTypeMenu = queryTypes.Select(x => new SelectListItem() {
                    Text = x
                })
            };

            return config;
        }
	}
}