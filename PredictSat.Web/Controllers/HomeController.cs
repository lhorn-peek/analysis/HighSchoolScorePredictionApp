﻿using PredictSat.Core.Data;
using PredictSat.Services.Queries.ComparePredictions;
using PredictSat.Web.Facade;
using System.Web.Mvc;

namespace IdentitySample.Controllers
{
    public class HomeController : Controller
    {
        //private ComparePredictionsReportFacade _reportFacade;

        //public HomeController(ComparePredictionsReportFacade reportFacade)
        //{
        //    _reportFacade = reportFacade;
        //}



        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";


            //var queryParams = new QueryParameters() { MinMax = true, UserId = "61bea314-e8ba-4f91-8662-03a562ac1bbe" };

            //var result = _reportFacade.ExecuteQuery(queryParams);

            //var s = "";


            return View();
        }
    }
}
