﻿using PredictSat.Core.Data;
using PredictSat.Services.Queries.ComparePredictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PredictSat.Web.Facade
{
    public class ComparePredictionsReportFacade
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly IQueryStrategy _queryStrategy;

        public ComparePredictionsReportFacade(IQueryProcessor queryProcessor, IQueryStrategy queryStrategy)
        {
            _queryProcessor = queryProcessor;
            _queryStrategy = queryStrategy;
        }


        public object ExecuteQuery(QueryParameters queryParameters)
        {
            if (queryParameters == null)
                throw new ArgumentNullException("queryParameters");

            if (String.IsNullOrEmpty(queryParameters.UserId))
                throw new ArgumentNullException("queryParameters.UserId");

            var query = _queryStrategy.CreateQuery(queryParameters);

            var result = _queryProcessor.Process(query);

            return result;
        }
    }
}