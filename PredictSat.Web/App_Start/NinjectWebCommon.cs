using PredictSat.Services.Predictions;
using PredictSat.Services.AzureML;
using PredictSat.Core;
using PredictSat.Core.Data;
using PredictSat.Core.Domain.Predictions;
using PredictSat.Data;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(PredictSat.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(PredictSat.Web.App_Start.NinjectWebCommon), "Stop")]

namespace PredictSat.Web.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using PredictSat.Services.Queries.ComparePredictions;
    using PredictSat.Web.Facade;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IDbContext>().To<PredictDbContext>();
            kernel.Bind<IRepository<SatPredictionInput>>().To<EfRepository<SatPredictionInput>>();
            kernel.Bind<IAzureMLWebServiceWrapper>().To<AzureMLWebService>();
            kernel.Bind<ISatPredictionResultParser>().To<SatPredictionResultParser>();
            kernel.Bind<ISatPredictionService>().To<SatPredictionService>();


            //Query Handling

            kernel.Bind<IQueryStrategy>().To<QueryStrategy>().WithConstructorArgument("factories", new IQueryFactory[] { 
                new ComparePredictionsQueryFactory(),
                new MinAndMaxPredictionScoresQueryFactory()
            });

            kernel.Bind<IQueryProcessor>().To<QueryProcessor>();

            kernel.Bind<ComparePredictionsReportFacade>().To<ComparePredictionsReportFacade>();
        }        
    }
}
