﻿using Moq;
using NUnit.Framework;
using PredictSat.Data;
using PredictSat.Services.Queries.ComparePredictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PredictSat.UnitTests
{
    [TestFixture]
    public class ComparePredictionsQueryFactoryTests
    {
        [Test]
        public void ShouldReturnComparePredictionsQuery()
        {
            var queryParams = new QueryParameters() { PredictionIds = new List<int> { 1, 4 } };

            var expectedQueryType = typeof(ComparePredictionsQuery);

            var factory = new ComparePredictionsQueryFactory();


            factory.AppliesTo(queryParams);

            Assert.That(factory.CreateQuery(), Is.TypeOf(expectedQueryType));
        }


        [Test]
        public void ShouldPassAppliesTo()
        {
            var queryParams = new QueryParameters() { PredictionIds = new List<int> { 1, 4 } };

            var factory = new ComparePredictionsQueryFactory();

            Assert.That(factory.AppliesTo(queryParams), Is.True);

        }

        [Test]
        public void ShouldFailAppliesTo()
        {
            var queryParams = new QueryParameters() { MinMax = true };

            var factory = new ComparePredictionsQueryFactory();

            Assert.That(factory.AppliesTo(queryParams), Is.False);

        }
    }
}
