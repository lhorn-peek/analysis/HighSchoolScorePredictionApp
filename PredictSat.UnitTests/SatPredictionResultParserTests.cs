﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using PredictSat.Services.Predictions;

namespace PredictSat.UnitTests
{
    [TestFixture]
    public class SatPredictionResultParserTests
    {
        [Test]
        public void ShouldThrowExceptionWhenIncorrectResultString()
        {
            var sut = new SatPredictionResultParser();
            var webServiceResponseString = "{\"Result\":\"\"}";

            Assert.Throws<Exception>(new TestDelegate(() => sut.Parse(webServiceResponseString)));
        }

        [Test]
        public void ShouldThrowExceptionWithCorrectMessage()
        {
            var sut = new SatPredictionResultParser();
            var incorrectwebServiceResponseString = "{\"Result\":\"\"}";

            var ex = Assert.Throws<Exception>(new TestDelegate(() => sut.Parse(incorrectwebServiceResponseString)));

            Assert.IsTrue(ex.Message.Contains("Unable to parse prediction web service result."));
        }

        [Test]
        public void ShouldParseCorrectResponseString()
        {
            var sut = new SatPredictionResultParser();
            var correctwebServiceResponseString = "{'Results':{'output1':{'type':'table','value':{'ColumnNames':['0'],'ColumnTypes':['String'],'Values':[['{\"type\": \"list\", \"value\": [{\"type\": \"list\", \"value\": [{\"type\": \"float\", \"value\": \"377.7\"}, {\"type\": \"float\", \"value\": \"384.3\"}, {\"type\": \"float\", \"value\": \"374.7\"}]}]}']]}},'output2':{'type':'table','value':{'ColumnNames':['Standard Output','Standard Error','Graphics'],'ColumnTypes':['String','String','String'],'Values':[['data:text/plain,','data:text/plain,',null]]}}},'ContainerAllocationDurationMs':1281,'ExecutionDurationMs':39539,'IsWarmContainer':false}";

            var result = sut.Parse(correctwebServiceResponseString);

            Assert.IsNotNull(result);
        }
    }
}
