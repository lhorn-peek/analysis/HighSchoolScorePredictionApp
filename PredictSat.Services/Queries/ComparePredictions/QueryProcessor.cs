﻿using PredictSat.Core.Data;
using PredictSat.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public sealed class QueryProcessor : IQueryProcessor
    {
        public QueryProcessor()
        {

        }

        public TResult Process<TResult>(IQuery<TResult> query)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));

            dynamic handler = QueryHandlerLocator.Current.Get(handlerType);

            return handler.Handle((dynamic)query);
        }


        //NEED to fix this. must return handlertype dynamically using reflection. ie: above...
        public object Process(IQuery query)
        {
            var genArg = query.GetType().GetInterface("IQuery`1").GetGenericArguments()[0];

            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), genArg);

            dynamic handler = QueryHandlerLocator.Current.Get(handlerType);

            return handler.Handle((dynamic)query);
        }
    }
}
