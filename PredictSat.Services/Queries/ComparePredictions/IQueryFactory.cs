﻿using PredictSat.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public interface IQueryFactory
    {
        IQuery CreateQuery();

        bool AppliesTo(QueryParameters parameters);
    }

    public interface IQueryFactory<TQuery, TResult> : IQueryFactory where TQuery : IQuery<TResult>
    {
        new IQuery<TResult> CreateQuery();

        
    }
}
