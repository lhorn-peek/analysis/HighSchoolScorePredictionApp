﻿using PredictSat.Core.Data;
using PredictSat.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class MinAndMaxPredictionScoresQueryHandler : IQueryHandler<MinAndMaxPredictionScoresQuery, MinMaxPredictionDTO[]>
    {
        private readonly IDbContext _db;

        public MinAndMaxPredictionScoresQueryHandler(IDbContext db)
        {
            _db = db;
        }



        public MinMaxPredictionDTO[] Handle(MinAndMaxPredictionScoresQuery query)
        {

            var predictions = _db.SqlQuery<MinMaxPredictionDTO>("exec [dbo].[MinMaxPredictions] '" + query.UserId + "'").ToArray();

            return predictions;
        }
    }
}
