﻿using PredictSat.Core.Data;
using PredictSat.Core.Domain.Predictions;
using PredictSat.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PredictSat.Services.Queries.ComparePredictions
{
    public class ComparePredictionsQueryHandler : IQueryHandler<ComparePredictionsQuery, PredictionDTO[]>
    {
        private readonly IDbContext _db;

        public ComparePredictionsQueryHandler(IDbContext db)
        {
            _db = db;
        }
        

        public PredictionDTO[] Handle(ComparePredictionsQuery query)
        {

            string sql = string.Format("select "+
	                              "i.[SchoolName] "+
                                  ",i.[ForYear] "+
                                  ",i.[NumberTestTakers] " +
                                  ",i.[PeerIndex] "+
                                  ",i.[PerformanceCategoryScore] "+
                                  ",i.[NumberOfClassesMean] "+
                                  ",i.[SchoolWideTeacherPupilRation] "+
                                  ",i.[AvgClassSizeMean] "+
                                  ",i.[ProgressCategoryScore] "+
                                  ",i.[SizeOfSmallestClass] "+
                                  ",i.[TotalRegisterMean] "+
                                  ",i.[EnviromentCategoryScore] "+
	                              ",o.[CriticalReadingMean] "+
                                  ",o.[MathematicsMean] "+
                                  ",o.[WritingMean] "+
                            "from predictioninputs i "+
                            "left join predictionresults o on o.Id = i.Id "+
                            "where i.Id in ({0}) and i.UserId = '{1}'", string.Join(",", query.PredictionIds), query.UserId);

            var predictions = _db.SqlQuery<PredictionDTO>(sql).ToArray();

            return predictions;
        }
    }
}
