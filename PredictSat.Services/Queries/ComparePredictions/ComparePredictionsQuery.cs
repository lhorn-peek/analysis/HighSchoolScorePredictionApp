﻿using PredictSat.Core.Data;
using PredictSat.Core.Domain.Predictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class ComparePredictionsQuery : IQuery<PredictionDTO[]>
    {
        public int[] PredictionIds { get; set; }

        public string UserId { get; set; }
    }
}
