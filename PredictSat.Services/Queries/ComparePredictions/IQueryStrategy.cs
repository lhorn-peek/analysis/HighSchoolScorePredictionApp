﻿using PredictSat.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public interface IQueryStrategy
    {
        
        IQuery CreateQuery(QueryParameters parameters);

    }
}
