﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class PredictionDTO
    {

        public string SchoolName { get; set; }

        public DateTime ForYear { get; set; }

        public int NumberTestTakers { get; set; }

        public double PeerIndex { get; set; }

        public double PerformanceCategoryScore { get; set; }

        public double NumberOfClassesMean { get; set; }

        public double SchoolWideTeacherPupilRation { get; set; }

        public double AvgClassSizeMean { get; set; }

        public double ProgressCategoryScore { get; set; }

        public double SizeOfSmallestClass { get; set; }

        public double TotalRegisterMean { get; set; }

        public double EnviromentCategoryScore { get; set; }

        public double CriticalReadingMean { get; set; }

        public double MathematicsMean { get; set; }

        public double WritingMean { get; set; }

        //public double avg_score { get; set; }
    }
}
