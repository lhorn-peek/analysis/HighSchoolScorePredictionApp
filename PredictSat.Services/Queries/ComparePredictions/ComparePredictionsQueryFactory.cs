﻿using PredictSat.Core.Data;
using PredictSat.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class ComparePredictionsQueryFactory : IQueryFactory<ComparePredictionsQuery, PredictionDTO[]>
    {
        
        private QueryParameters parameters;

        public ComparePredictionsQueryFactory()
        {
            
        }

        IQuery IQueryFactory.CreateQuery()
        {
            return this.CreateQuery();
        }

        public IQuery<PredictionDTO[]> CreateQuery()
        {
            if (this.parameters == null)
                throw new ArgumentNullException("compareQueryParameters");

            return new ComparePredictionsQuery() { 
                PredictionIds = this.parameters.PredictionIds.ToArray(),
                UserId = this.parameters.UserId
            };
        }

        public bool AppliesTo(QueryParameters parameters)
        {
            if(parameters.PredictionIds != null)
            {
                if(parameters.PredictionIds.Count > 0)
                {
                    this.parameters = parameters;
                    return true;
                }
                
            }

            return false;
        }
    }
}
