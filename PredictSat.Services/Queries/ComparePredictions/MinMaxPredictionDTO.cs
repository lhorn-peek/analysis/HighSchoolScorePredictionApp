﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class MinMaxPredictionDTO : PredictionDTO
    {
        public double avg_score { get; set; }
    }
}
