﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class QueryParameters
    {
        public QueryParameters()
        {
            PredictionIds = new List<int>();
        }

        public List<int> PredictionIds { get; set; }

        public bool MinMax { get; set; }

        public string UserId  { get; set; }
    }
}
