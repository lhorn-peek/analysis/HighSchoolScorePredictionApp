﻿using PredictSat.Core.Data;
using PredictSat.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class MinAndMaxPredictionScoresQueryFactory : IQueryFactory<MinAndMaxPredictionScoresQuery, MinMaxPredictionDTO[]>
    {
        private QueryParameters parameters;

        public MinAndMaxPredictionScoresQueryFactory()
        {
        }

        IQuery IQueryFactory.CreateQuery()
        {
            return this.CreateQuery();
        }

        public IQuery<MinMaxPredictionDTO[]> CreateQuery()
        {
            if (this.parameters == null)
                throw new ArgumentNullException("minMaxQueryParameters");

            return new MinAndMaxPredictionScoresQuery() { 
                MinMaxPredictionsScores = this.parameters.MinMax ,
                UserId = this.parameters.UserId
            };
        }

        public bool AppliesTo(QueryParameters parameters)
        {
            if (parameters.MinMax)
            {
                this.parameters = parameters;
                return true;
            }

            return false;
        }
    }
}
