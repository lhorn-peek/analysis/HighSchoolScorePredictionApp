﻿using PredictSat.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Queries.ComparePredictions
{
    public class QueryStrategy : IQueryStrategy
    {
        private readonly IQueryFactory[] _factories;

        public QueryStrategy(IQueryFactory[] factories)
        {
            _factories = factories;
        }

        public IQuery CreateQuery(QueryParameters parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException("queryParameters");

            var queryFactory = _factories.FirstOrDefault(f => f.AppliesTo(parameters));

            if (queryFactory == null)
                throw new Exception("type not registered");

            return queryFactory.CreateQuery();
        }
    }
}
