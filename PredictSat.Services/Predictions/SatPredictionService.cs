﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PredictSat.Core.Data;
using PredictSat.Core.Domain.Predictions;
using PredictSat.Services.AzureML;
using PredictSat.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Predictions
{
    public class SatPredictionService : ISatPredictionService
    {

        private IRepository<SatPredictionInput> _predictionRepository;
        private IAzureMLWebServiceWrapper _azureWebService;
        private ISatPredictionResultParser _resultParser;

        public SatPredictionService(IRepository<SatPredictionInput> predictionRepository, 
            IAzureMLWebServiceWrapper azureWebService,
            ISatPredictionResultParser resultParser)
        {
            _predictionRepository = predictionRepository;
            _azureWebService = azureWebService;
            _resultParser = resultParser;
        }


        public void DeletePrediction(SatPredictionInput prediction)
        {
            if (prediction == null)
                throw new ArgumentNullException("prediction");

            _predictionRepository.Delete(prediction);
        }

        public void DeletePredictionById(int id)
        {
            if (id == 0)
                throw new ArgumentException("predictionId zero.");

            var prediction = _predictionRepository.GetById(id);

            _predictionRepository.Delete(prediction);
        }

        public IList<SatPredictionInput> GetAllPrections(string userId)
        {
            if (String.IsNullOrEmpty(userId))
                return null;

            var query = _predictionRepository.Table;
            query = query.Where(p => p.UserId == userId);
            query = query.IncludeProperties(p => p.PredictionResult);

            var predictions = query.ToList();

            return predictions;
        }

        public SatPredictionInput GetPredictionInputAndResultById(int id)
        {
            if (id == 0)
                return null;

            var query = _predictionRepository.Table;
            query = query.Where(p => p.Id == id);
            query = query.IncludeProperties(p => p.PredictionResult);

            var prediction = query.FirstOrDefault();

            return prediction;
        }

        public SatPredictionInput GetPredictionById(int id)
        {
            if (id == 0)
                return null;

            return _predictionRepository.GetById(id);
        }

        public void InsertPrediction(SatPredictionInput prediction)
        {
            if (prediction == null)
                throw new ArgumentNullException("predictionInput");

            if (prediction.PredictionResult == null)
                throw new ArgumentNullException("predictionResult");

            _predictionRepository.Insert(prediction);
        }

        public SatPredictionResult MakePrediction(SatPredictionInput predictionInput)
        {
            if (predictionInput == null)
                throw new ArgumentNullException("predictionInput");

            var predictParams = new PredictServiceParameters("TxnTBe55LcfKsyEthozoROxihJoY9dDtfYCSScWX/VrKaUlrYNLVIBZK/dJPTpkEohIKxTVgAfS8cBL4HsF5dg==");

            predictParams.InputColumnNames = new string[] 
            { 
                "num_test_takers", 
                "peer_indx", 
                "perf_cat_score", 
                "num_classes_mean", 
                "school_wide_pup_teach_ration", 
                "avg_class_size_mean", 
                "prog_cat_score", 
                "size_smallest_class", 
                "tot_register_mean", 
                "env_cat_score" 
            };
            predictParams.InputValues = new string[,] { 
            { 
                predictionInput.NumberTestTakers.ToString(), 
                predictionInput.PeerIndex.ToString(), 
                predictionInput.PerformanceCategoryScore.ToString(), 
                predictionInput.NumberOfClassesMean.ToString(), 
                predictionInput.SchoolWideTeacherPupilRation.ToString(), 
                predictionInput.AvgClassSizeMean.ToString(), 
                predictionInput.ProgressCategoryScore.ToString(), 
                predictionInput.SizeOfSmallestClass.ToString(), 
                predictionInput.TotalRegisterMean.ToString(), 
                predictionInput.EnviromentCategoryScore.ToString() 
            }, };

            string strResult = _azureWebService.ExecutePredictService(predictParams);
            var predictionResult = _resultParser.Parse(strResult);

            return predictionResult;
        }


        private bool _validatePredictionInput(SatPredictionInput input)
        {
            if (input == null)
                return false;



            return true;
        }
    }
}
