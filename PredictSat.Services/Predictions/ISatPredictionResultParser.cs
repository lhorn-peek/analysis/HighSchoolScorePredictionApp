﻿using PredictSat.Core.Domain.Predictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Predictions
{
    public interface ISatPredictionResultParser
    {
        SatPredictionResult Parse(string webServiceResult);
    }
}
