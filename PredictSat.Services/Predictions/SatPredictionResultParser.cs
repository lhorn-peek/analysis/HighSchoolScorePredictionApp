﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PredictSat.Core.Domain.Predictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Predictions
{
    public class SatPredictionResultParser : ISatPredictionResultParser
    {

        public SatPredictionResult Parse(string webServiceResult)
        {
            try
            {
                if (string.IsNullOrEmpty(webServiceResult))
                    throw new ArgumentException("Prediction web service response string is null or empty.");

                var result = new SatPredictionResult();

                var objResult = JObject.Parse(webServiceResult);
                string data = (string)objResult["Results"]["output1"]["value"]["Values"][0][0];
                var objResultData = JObject.Parse(data);

                result.CriticalReadingMean = (double)objResultData["value"][0]["value"][0]["value"];
                result.WritingMean = (double)objResultData["value"][0]["value"][1]["value"];
                result.MathematicsMean = (double)objResultData["value"][0]["value"][2]["value"];

                return result;
            }
            catch (Exception ex)
            {
                var msg = string.Format("Unable to parse prediction web service result. Error:{0}", ex.Message);
                var error = new Exception(msg, ex);

                throw error;
            }
        }
    }
}
