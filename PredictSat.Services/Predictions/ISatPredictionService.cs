﻿using PredictSat.Core.Domain.Predictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Predictions
{
    public interface ISatPredictionService
    {
        void DeletePrediction(SatPredictionInput prediction);

        void DeletePredictionById(int id);

        SatPredictionInput GetPredictionInputAndResultById(int id);

        IList<SatPredictionInput> GetAllPrections(string userId);

        SatPredictionInput GetPredictionById(int id);

        void InsertPrediction(SatPredictionInput prediction);

        SatPredictionResult MakePrediction(SatPredictionInput predictionInput);
    }
}
