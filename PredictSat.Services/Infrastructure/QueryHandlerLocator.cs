﻿using Ninject;
using PredictSat.Core.Data;
using PredictSat.Core.Infrastructure;
using PredictSat.Data;
using PredictSat.Services.Queries.ComparePredictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.Infrastructure
{
    internal class QueryHandlerLocator
    {
        private static IQueryHandlerLocator queryLocator;

        static QueryHandlerLocator()
        {
            queryLocator = new DefaultQueryHandlerLocator();
        }

        public static IQueryHandlerLocator Current
        {
            get
            {
                return queryLocator;
            }
        }
   

        private sealed class DefaultQueryHandlerLocator : IQueryHandlerLocator
        {
            private readonly IKernel kernel;

            public DefaultQueryHandlerLocator()
            {
                kernel = new StandardKernel();
                LoadBindings();
            }

            public T Get<T>()
            {
                try
                {
                    return kernel.Get<T>();
                }
                catch (ActivationException exception)
                {
                    throw new Exception("QueryHandler type not registered.", exception);
                }
            }

            public object Get(Type type)
            {
                try
                {
                    return kernel.Get(type);
                }
                catch (ActivationException exception)
                {
                    throw new Exception("QueryHandler type not registered.", exception);
                }
            }

            private void LoadBindings()
            {
                kernel.Bind<IDbContext>().To<PredictDbContext>();

                kernel.Bind<IQueryHandler<ComparePredictionsQuery, PredictionDTO[]>>().To<ComparePredictionsQueryHandler>();
                kernel.Bind<IQueryHandler<MinAndMaxPredictionScoresQuery, MinMaxPredictionDTO[]>>().To<MinAndMaxPredictionScoresQueryHandler>();
            }
        }
    }
}
