﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace PredictSat.Services.AzureML
{
    public class AzureMLWebService : IAzureMLWebServiceWrapper
    {
        //private PredictServiceParameters _serviceParameters;

        //public AzureMLWebService(PredictServiceParameters serviceParameters)
        //{
        //    _serviceParameters = serviceParameters;
        //}

        public AzureMLWebService()
        {

        }

        public string ExecutePredictService(PredictServiceParameters _serviceParameters)
        {
            using (var client = new HttpClient())
            {
                //request
                var scoreRequest = new
                {
                    Inputs = new Dictionary<string, StringTable>()
                    {{
                        "input1",
                        new StringTable()
                        {
                            ColumnNames = _serviceParameters.InputColumnNames,
                            Values = _serviceParameters.InputValues
                        }
                    
                    }},
                    GlobalParameters = new Dictionary<string, string>() { }
                };
                
                //execution
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _serviceParameters.ApiKey);
                client.BaseAddress = new Uri("https://ussouthcentral.services.azureml.net/workspaces/8b9323b2ee374331954dbd401bdb746e/services/0ec74c5fa9954da2b29d36755071e983/execute?api-version=2.0&details=true");

                HttpResponseMessage response = client.PostAsJsonAsync("", scoreRequest).Result;

                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    return result;
                }
                else
                {
                    string responseError = string.Format("The request failed with status code: {0}. ", response.StatusCode);
                    responseError += response.Content.ReadAsStringAsync().Result;

                    return responseError;
                }
            }

        }
    }

    public class StringTable
    {
        public string[] ColumnNames { get; set; }
        public string[,] Values { get; set; }
    }
}
