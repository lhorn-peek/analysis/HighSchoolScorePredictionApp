﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.AzureML
{
    public class PredictServiceParameters
    {
        private string _apiKey;

        public PredictServiceParameters(string apiKey)
        {
            _apiKey = apiKey;
            //InputColumnNames = new List<string>();
            //InputValues = new List<string>();
        }


        public string ApiKey
        {
            get
            {
                return _apiKey;
            }
        }


        public string[] InputColumnNames { get; set; }

        public string[,] InputValues { get; set; }
    }
}
