﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Services.AzureML
{
    public interface IAzureMLWebServiceWrapper
    {
        string ExecutePredictService(PredictServiceParameters _serviceParameters);
    }
}
