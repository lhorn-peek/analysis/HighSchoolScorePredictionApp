﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Core.Infrastructure
{
    public interface IQueryHandlerLocator
    {
        T Get<T>();

        object Get(Type type);
        
    }
}
