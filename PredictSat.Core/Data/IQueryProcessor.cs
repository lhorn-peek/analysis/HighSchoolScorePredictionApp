﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Core.Data
{
    public interface IQueryProcessor
    {
        TResult Process<TResult>(IQuery<TResult> query);

        object Process(IQuery query);
    }
}
