﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Core.Domain.Predictions
{
    public class SatPredictionInput : BaseEntity
    {
        public string UserId { get; set; }

        public string SchoolName { get; set; }

        public DateTime ForYear { get; set; }

        public int NumberTestTakers { get; set; }

        public double PeerIndex { get; set; }

        public double PerformanceCategoryScore { get; set; }

        public double NumberOfClassesMean { get; set; }

        public double SchoolWideTeacherPupilRation { get; set; }

        public double AvgClassSizeMean { get; set; }

        public double ProgressCategoryScore { get; set; }

        public double SizeOfSmallestClass { get; set; }

        public double TotalRegisterMean { get; set; }

        public double EnviromentCategoryScore { get; set; }

        public virtual SatPredictionResult PredictionResult { get; set; }

    }
}
