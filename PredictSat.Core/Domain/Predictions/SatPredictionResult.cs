﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PredictSat.Core.Domain.Predictions
{
    public class SatPredictionResult : BaseEntity
    {
        public double CriticalReadingMean { get; set; }

        public double MathematicsMean { get; set; }

        public double WritingMean { get; set; }

        public virtual SatPredictionInput PredictionInput { get; set; }
    }
}
