﻿using Moq;
using NUnit.Framework;
using PredictSat.Services.AzureML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using PredictSat.Services.Predictions;
using PredictSat.Core.Domain.Predictions;
using PredictSat.Core.Data;

namespace PredictSat.IntegrationTests
{
    [TestFixture]
    public class SatPredictionServiceSubSystemTests
    {
        [Test]
        public void ShouldReturnCorrectPrediction()
        {
            var fakeWebServiceResponseString = "{'Results':{'output1':{'type':'table','value':{'ColumnNames':['0'],'ColumnTypes':['String'],'Values':[['{\"type\": \"list\", \"value\": [{\"type\": \"list\", \"value\": [{\"type\": \"float\", \"value\": \"377.7\"}, {\"type\": \"float\", \"value\": \"384.3\"}, {\"type\": \"float\", \"value\": \"374.7\"}]}]}']]}},'output2':{'type':'table','value':{'ColumnNames':['Standard Output','Standard Error','Graphics'],'ColumnTypes':['String','String','String'],'Values':[['data:text/plain,','data:text/plain,',null]]}}},'ContainerAllocationDurationMs':1281,'ExecutionDurationMs':39539,'IsWarmContainer':false}";

            var fakePredictionWebService = new Mock<IAzureMLWebServiceWrapper>();
            fakePredictionWebService.Setup(x => x.ExecutePredictService(It.IsAny<PredictServiceParameters>()))
                .Returns(fakeWebServiceResponseString);

            //fake repo
            var fakeRepo = new Mock<IRepository<SatPredictionInput>>();

            var kernel = new StandardKernel();

            kernel.Bind<IAzureMLWebServiceWrapper>().ToConstant(fakePredictionWebService.Object);

            kernel.Bind<ISatPredictionResultParser>().To<SatPredictionResultParser>();

            kernel.Bind<IRepository<SatPredictionInput>>().ToConstant(fakeRepo.Object);

            var sut = kernel.Get<SatPredictionService>();

            var predictionInput = new SatPredictionInput()
            {
                AvgClassSizeMean = 0,
                EnviromentCategoryScore = 0,
                NumberOfClassesMean = 0,
                NumberTestTakers = 0,
                PeerIndex = 0,
                PerformanceCategoryScore = 0,
                ProgressCategoryScore = 0,
                SchoolWideTeacherPupilRation = 0,
                SizeOfSmallestClass = 0,
                TotalRegisterMean = 0
            };

            var result = sut.MakePrediction(predictionInput);

            Assert.IsNotNull(result);
            Assert.That(result.CriticalReadingMean, Is.GreaterThan(0.0));
            Assert.That(result.WritingMean, Is.GreaterThan(0.0));
            Assert.That(result.MathematicsMean, Is.GreaterThan(0.0));
        }
    }
}
