# HighSchoolScorePredictionApp

'HighSchoolScorePredictionApp' is a web application used to make predictions on a schools' final Reading, Writing and Maths grades.

The application makes its predictions by sending input parameters to a Machine Learning Model hosted in Azure. The Python scripts are in the repository 'HighSchoolPredictionApp_PYTHON_MODEL'.

The end user will make a prediction on a schools scores by inputing certain data such as: size of smallest class; enviroment score; etc. Once the end user has entered all required data, he/she can view the prediction with the option of saving the results. Predictions can also be compared to other predictions using the online report. 
