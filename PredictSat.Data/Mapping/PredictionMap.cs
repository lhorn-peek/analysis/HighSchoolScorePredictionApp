﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PredictSat.Core.Domain.Predictions;
using System.Data.Entity.ModelConfiguration;

namespace PredictSat.Data.Mapping
{
    public class PredictionMap : EntityTypeConfiguration<SatPredictionInput>
    {
        public PredictionMap()
        {
            this.ToTable("PredictionInputs");

            //this.HasKey(p => p.Id);

            this.Property(t => t.UserId).IsRequired();

            this.Property(t => t.NumberOfClassesMean).IsRequired();
            this.Property(t => t.NumberTestTakers).IsRequired();
            this.Property(t => t.PeerIndex).IsRequired();
            this.Property(t => t.PerformanceCategoryScore).IsRequired();
            this.Property(t => t.ProgressCategoryScore).IsRequired();
            this.Property(t => t.SchoolWideTeacherPupilRation).IsRequired();
            this.Property(t => t.SizeOfSmallestClass).IsRequired();
            this.Property(t => t.TotalRegisterMean).IsRequired();
            this.Property(t => t.AvgClassSizeMean).IsRequired();
            this.Property(t => t.EnviromentCategoryScore).IsRequired();

            this.HasOptional(t => t.PredictionResult)
                .WithRequired(t => t.PredictionInput).WillCascadeOnDelete(true);
            
        }
    }
}
