﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PredictSat.Core.Domain.Predictions;
using System.Data.Entity.ModelConfiguration;

namespace PredictSat.Data.Mapping
{
    public class PredictionResultMap : EntityTypeConfiguration<SatPredictionResult>
    {
        public PredictionResultMap()
        {
            this.ToTable("PredictionResults");

            this.Property(t => t.CriticalReadingMean).IsRequired();
            this.Property(t => t.MathematicsMean).IsRequired();
            this.Property(t => t.WritingMean).IsRequired();

        }
    }
}
