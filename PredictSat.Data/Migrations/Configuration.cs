namespace PredictSat.Data.Migrations
{
    using PredictSat.Core.Domain.Predictions;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PredictSat.Data.PredictDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PredictSat.Data.PredictDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var prediction = new SatPredictionInput()
            {
                AvgClassSizeMean = 0,
                EnviromentCategoryScore = 0,
                ForYear = DateTime.Now,
                NumberOfClassesMean = 0,
                NumberTestTakers = 0,
                PeerIndex = 0,
                PerformanceCategoryScore = 0,
                ProgressCategoryScore = 0,
                SchoolName = "High School X",
                SchoolWideTeacherPupilRation = 0,
                SizeOfSmallestClass = 0,
                TotalRegisterMean = 0,
                UserId = "61bea314-e8ba-4f91-8662-03a562ac1bbe"
            };

            prediction.PredictionResult = new SatPredictionResult()
            {
                CriticalReadingMean = 377.7,
                WritingMean = 384.3,
                MathematicsMean = 374.7
            };

            context.PredictionInputs.AddOrUpdate(prediction);
        }
    }
}
