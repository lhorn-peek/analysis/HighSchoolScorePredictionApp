namespace PredictSat.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cascadeOnDelete_PredictionInput : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PredictionResults", "Id", "dbo.PredictionInputs");
            AddForeignKey("dbo.PredictionResults", "Id", "dbo.PredictionInputs", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PredictionResults", "Id", "dbo.PredictionInputs");
            AddForeignKey("dbo.PredictionResults", "Id", "dbo.PredictionInputs", "Id");
        }
    }
}
