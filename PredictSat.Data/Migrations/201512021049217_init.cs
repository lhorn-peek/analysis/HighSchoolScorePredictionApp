namespace PredictSat.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PredictionInputs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        SchoolName = c.String(),
                        ForYear = c.DateTime(nullable: false),
                        NumberTestTakers = c.Int(nullable: false),
                        PeerIndex = c.Double(nullable: false),
                        PerformanceCategoryScore = c.Double(nullable: false),
                        NumberOfClassesMean = c.Double(nullable: false),
                        SchoolWideTeacherPupilRation = c.Double(nullable: false),
                        AvgClassSizeMean = c.Double(nullable: false),
                        ProgressCategoryScore = c.Double(nullable: false),
                        SizeOfSmallestClass = c.Double(nullable: false),
                        TotalRegisterMean = c.Double(nullable: false),
                        EnviromentCategoryScore = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PredictionResults",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        CriticalReadingMean = c.Double(nullable: false),
                        MathematicsMean = c.Double(nullable: false),
                        WritingMean = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PredictionInputs", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PredictionResults", "Id", "dbo.PredictionInputs");
            DropIndex("dbo.PredictionResults", new[] { "Id" });
            DropTable("dbo.PredictionResults");
            DropTable("dbo.PredictionInputs");
        }
    }
}
